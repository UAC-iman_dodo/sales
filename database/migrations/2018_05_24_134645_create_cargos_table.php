<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->increments('id_cargo');
            $table->integer('office_id');
            $table->integer('office_id_to');
            $table->integer('user_id');
            $table->integer('status')->nullable();
            $table->text('from');
            $table->text('for'); 
            $table->text('address_for'); 
            $table->dateTime('due_date')->nullable(); 
            $table->dateTime('in_date'); 
            $table->text('receipt')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
