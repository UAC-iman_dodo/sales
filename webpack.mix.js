let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 let folder = 'resources/assets/pike/';

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.copy(folder+'js/bootstrap.min.js', 'public/pike/js')
	.copy(folder+'js/jquery.min.js', 'public/pike/js/jquery.min.js')
	.copy(folder+'js/jquery-ui.min.js', 'public/pike/js/jquery-ui.min.js')
	.copy(folder+'js/jquery.blockUI.js', 'public/pike/js/jquery.blockUI.js')
	.copy(folder+'js/jquery.nicescroll.js', 'public/pike/js/jquery.nicescroll.js')
	.copy(folder+'js/jquery.scrollTo.min.js', 'public/pike/js/jquery.scrollTo.min.js')
    .copy(folder+'css/bootstrap.min.css', 'public/pike/css/bootstrap.min.css');

mix.copy(folder+'font-awesome', 'public/pike/font-awesome')
	.copy(folder+'css/style.css', 'public/pike/css/style.css')

mix.copy(folder+'js/modernizr.min.js', 'public/pike/js/modernizr.min.js')
	.copy(folder+'js/moment.min.js', 'public/pike/js/moment.min.js')
	.copy(folder+'js/popper.min.js', 'public/pike/js/popper.min.js');

mix.copy(folder+'js/detect.js', 'public/pike/js/detect.js')
	.copy(folder+'js/fastclick.js', 'public/pike/js/fastclick.js')
	.copy(folder+'js/pikeadmin.js', 'public/pike/js/pikeadmin.js');

mix.copy(folder+'plugins/fullcalendar/fullcalendar.min.js', 'public/pike/plugins/fullcalendar/fullcalendar.min.js');

mix.copy(folder+'plugins/switchery', 'public/pike/plugins/switchery');
