-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 02:11 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sales_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id_cargo` int(10) UNSIGNED NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_id_to` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `from` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `for` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_for` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `in_date` datetime NOT NULL,
  `receipt` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cargos`
--

INSERT INTO `cargos` (`id_cargo`, `office_id`, `office_id_to`, `user_id`, `status`, `from`, `for`, `address_for`, `due_date`, `in_date`, `receipt`, `created_at`, `updated_at`) VALUES
(28, 1, 2, 1, 1, 'pengirim 1', 'penerima 1', 'alamat penerima1', '2018-06-18 00:00:00', '2018-06-04 18:01:19', '28/1/18/06', '2018-06-04 11:01:19', '2018-06-04 11:04:53'),
(29, 1, 2, 1, 1, 'pengirim 2', 'penerima 2', 'alamat penerima 2', '2018-06-11 00:00:00', '2018-06-05 06:25:27', '29/1/18/06', '2018-06-04 23:25:27', '2018-06-04 23:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE `goods` (
  `id_goods` int(10) UNSIGNED NOT NULL,
  `cargo_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `goods`
--

INSERT INTO `goods` (`id_goods`, `cargo_id`, `description`, `quantity`, `price`, `service_id`, `weight`, `created_at`, `updated_at`) VALUES
(17, 28, 'barang 1', 1, 5000, 3, 1000, '2018-06-04 11:01:19', '2018-06-04 11:01:19'),
(18, 28, 'barang 2', 4, 15000, 2, 500, '2018-06-04 11:01:19', '2018-06-04 11:01:19'),
(19, 28, 'barang 3', 2, 10000, 1, 750, '2018-06-04 11:01:19', '2018-06-04 11:01:19'),
(20, 28, 'barang 44', 3, 15000, 2, 250, '2018-06-04 11:01:19', '2018-06-04 11:01:19'),
(21, 29, 'barang 1', 1, 10000, 1, 1000, '2018-06-04 23:25:27', '2018-06-04 23:25:27'),
(22, 29, 'barang 2', 2, 15000, 2, 500, '2018-06-04 23:25:27', '2018-06-04 23:25:27'),
(23, 29, 'barang 3', 1, 5000, 3, 2000, '2018-06-04 23:25:27', '2018-06-04 23:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2018_05_24_134816_create_provinces_table', 1),
(5, '2018_05_24_134821_create_cities_table', 1),
(7, '2018_05_25_122713_create_locations_table', 1),
(48, '2018_05_24_134645_create_cargos_table', 2),
(49, '2018_05_24_134759_create_goods_table', 2),
(50, '2018_05_24_134807_create_offices_table', 2),
(51, '2018_05_24_134828_create_office_statuses_table', 2),
(52, '2018_05_29_091151_create_services_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id_office` int(10) UNSIGNED NOT NULL,
  `office_status_id` int(11) NOT NULL,
  `office` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellular` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id_office`, `office_status_id`, `office`, `address`, `contact`, `fax`, `phone`, `cellular`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cabang1', 'Alamat Cabang 1', '081910641800', '------', 'User 1', '089992019992', '2018-05-30 05:00:44', '2018-06-04 10:59:26'),
(2, 1, 'Cabang 2', 'Alamat Cabang 2', '081910641422', '-----', 'User 2', '0892011233211', '2018-06-02 19:57:30', '2018-06-04 11:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `office_statuses`
--

CREATE TABLE `office_statuses` (
  `id_status` int(10) UNSIGNED NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `office_statuses`
--

INSERT INTO `office_statuses` (`id_status`, `status`) VALUES
(1, 'Kantor Perwakilan'),
(2, 'Agen Utama'),
(3, 'Sub Agen');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id_services` int(10) UNSIGNED NOT NULL,
  `services` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `prices` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id_services`, `services`, `prices`, `created_at`, `updated_at`) VALUES
(1, 'Kilats', 10000, NULL, '2018-06-04 05:46:59'),
(2, 'Express', 15000, NULL, NULL),
(3, 'Biasa', 5000, '2018-06-04 05:47:35', '2018-06-04 05:47:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id_goods`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id_office`);

--
-- Indexes for table `office_statuses`
--
ALTER TABLE `office_statuses`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_services`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id_cargo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
  MODIFY `id_goods` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id_office` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `office_statuses`
--
ALTER TABLE `office_statuses`
  MODIFY `id_status` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id_services` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
