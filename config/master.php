<?php

return [
    'host_url' => env('UA_HOST_URL', 'http://localhost:8000/server'),
    'broker_id' => env('UA_BROKER_ID', 4213221),
    'broker_secret' => env('UA_BROKER_SECRET', '05698aad3b2e026ccfb0e8b8d231208c36d2d3b7'),
    'uri' => [
        'profile' => 'profile',
        'login' => 'login',
        'logout' => 'logout',
    ]
];