<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(array('middleware'=>'AuthSso'), function(){
	Route::get('/', 'Freight@load');

	Route::get('/freight/goods', 'Freight@load')->name('load');
	Route::get('/freight/detail/{id}', 'Freight@show')->name('detail');

	Route::get('/freight/register', 'Freight@register')->name('register');
	Route::post('/freight/insert', 'Freight@add')->name('add');

	Route::get('/freight/received/{id}', 'Freight@received')->name('received');

	Route::get('/branch', 'Branch@load')->name('load_branch');
	Route::get('/branch/detail/{id}', 'Branch@show')->name('detail_branch');

	Route::get('/branch/register', 'Branch@add')->name('register_branch');
	Route::post('/branch/insert', 'Branch@insert')->name('add_branch');

	Route::get('/branch/edit/{id}', 'Branch@edit')->name('edit_branch');
	Route::post('/branch/update/{id}', 'Branch@update')->name('update_branch');

	Route::get('/service', 'Freight@service')->name('load_service');

	Route::get('/service/add', 'Freight@add_service')->name('add_service');
	Route::post('/service/insert', 'Freight@insert_service')->name('insert_service');

	Route::get('/service/edit/{id}', 'Freight@edit_service')->name('edit_service');
	Route::post('/service/update/{id}', 'Freight@update_service')->name('update_service');
});