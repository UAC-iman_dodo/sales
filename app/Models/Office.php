<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    
    protected $table = 'offices';
    protected $primaryKey = 'id_office';

    public function office_status(){
    	return $this->hasOne('App\Models\office_status', 'status_id', 'id_status');
    }

    public function Cargo(){
    	return $this->belongsTo('App\Models\Cargo');
    }

    public function Cargo_to(){
    	return $this->belongsTo('App\Models\Cargo');
    }
}
