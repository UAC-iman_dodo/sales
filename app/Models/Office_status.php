<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office_status extends Model
{
    protected $table = 'office_statuses';
    protected $primaryKey = 'id_status';

    public function office(){
    	return $this->belongsTo('App\Models\office', 'status_id', 'id_status');
    }
}
