<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    
    protected $table = 'goods';
    protected $primaryKey = 'id_office';

    public function cargo(){
    	return $this->belongsTo('App\Models\cargo', 'cargo_id', 'id_cargo');
    }
}
