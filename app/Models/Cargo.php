<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'cargos';
    protected $primaryKey = 'id_cargo';


    public function goods(){
    	return $this->hasMany('App\Models\Goods', 'cargo_id', 'id_cargo');
    }

    public function office(){
        return $this->hasOne('App\Models\Office', 'id_office', 'office_id');
    }
    public function office_to(){
    	return $this->hasOne('App\Models\Office', 'id_office', 'office_id_to');
    }

    public function service(){
    	return $this->hasOne('App\Models\Service', 'service_id', 'id_service');
    }
}
