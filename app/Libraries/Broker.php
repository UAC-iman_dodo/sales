<?php

namespace App\Libraries;

use Jasny\SSO\Broker as JasnyBroker;
use Jasny\SSO\Exception as SSOException;
use Jasny\SSO\NotAttachedException;

class Broker extends JasnyBroker
{
    protected $authenticated_user;

    public function __construct()
    {
        parent::__construct(
            config('master.host_url'),
            config('master.broker_id'),
            config('master.broker_secret'),
            (5 * 3600)
        );
    }

    protected function requestUserInfo()
    {
        try {
            if ($user = $this->getUserInfo()) {
                $this->authenticated_user = $user;
            }
        } catch (SSOException $e) {
            $this->errors[] = $e->getMessage();
        } catch (NotAttachedException $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    public function check()
    {
        $this->requestUserInfo();
        return !empty($this->user());
    }

    public function user()
    {
        return $this->authenticated_user;
    }

    public function getServerURL($uri = '', $params = [])
    {
        if (!isset($params['access_token'])) {
            $params['access_token'] = $this->getSessionID();
        }
        return $this->url . '/' . $uri . '?' . http_build_query($params);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
