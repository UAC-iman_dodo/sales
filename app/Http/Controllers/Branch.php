<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Office;
use App\Models\Office_status;
use DB;

class Branch extends Controller
{
    //
    public function load(){
        $user = request()->auth_user;
        $office = Office::leftJoin('office_statuses', 'office_statuses.id_status', '=', 'offices.office_status_id')->get();
        return view('branch', compact('office', 'user'));
    }

    public function show($id){
        $user = request()->auth_user;
        $office = Office::leftJoin('office_statuses', 'office_statuses.id_status', '=', 'offices.office_status_id')
                    ->find($id);
        // load user
        return view('detail', compact('office', 'user'));
    }

    public function add(){
        $user = request()->auth_user;
        $subtitle = 'insert';
        $action = route('add_branch');
        $datas = Office_status::get();
        $status = array();
        foreach ($datas as $row) {
            $status[$row->id_status] = $row->status;
        }
        return view('crud_branch', compact('subtitle','action', 'status', 'user'));
    }

    public function insert(Request $request){
        DB::beginTransaction();
        try {  

            $office = new Office;
            $office->office     = $request->input('branch');
            $office->office_status_id     = $request->input('status');
            $office->address    = $request->input('address');
            $office->contact    = $request->input('contact');
            $office->fax        = $request->input('fax');
            $office->phone      = $request->input('phone');
            $office->cellular   = $request->input('cellular');

            $office->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('load_branch');
    }

    public function edit($id){
        $user = request()->auth_user;

        $office = Office::leftJoin('office_statuses', 'office_statuses.id_status', '=', 'offices.office_status_id')->find($id);
        $subtitle = 'update';
        $datas = Office_status::get();
        $status = array();
        foreach ($datas as $row) {
            $status[$row->id_status] = $row->status;
        }
        $action = route('update_branch', ['id'=>$id] );
        return view('crud_branch', compact('office','subtitle','action','status', 'user'));
    }

    public function update(Request $request, $id){
        
        DB::beginTransaction();
        try {  

            $office = Office::find($id);

            $office->office     = $request->input('branch');
            $office->office_status_id     = $request->input('status');
            $office->address    = $request->input('address');
            $office->contact    = $request->input('contact');
            $office->fax        = $request->input('fax');
            $office->phone      = $request->input('phone');
            $office->cellular   = $request->input('cellular');

            $office->save();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }

        return redirect()->route('load_branch');
    }
}
