<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Models\Location;
use App\Models\Goods;
use App\Models\Office;
use App\Models\Service;
use Carbon\Carbon;
use DB;

class Freight extends Controller
{
    public function load(){
        $user = request()->auth_user;
        $cargo = Cargo::with(['goods','office','office_to'])->get();
        return view('views', compact('cargo', 'user'));

    }

    public function show($id){
        $user = request()->auth_user;
        $cargo = Cargo::with('goods')
                    ->find($id);
        return view('details', compact('cargo', 'user'));
    }

    public function register(){
        $user = request()->auth_user;
        $subtitle = 'insert';
        $action = route('add');
        $offices = Office::get();
        $services = Service::get();
        $office = array();
        $service = array();
        foreach ($offices as $row) {
            $office[$row->id_office] = $row->address;
        }
        foreach ($services as $row) {
            $service[json_encode($row)] = $row->services;
        }
        return view('crud', compact('subtitle', 'action', 'user', 'office', 'service','services'));
    }

    public function add(Request $request){
        $user = request()->auth_user;
        DB::beginTransaction();
        try {  

            $cargo = new Cargo;

            $cargo->user_id         = $user['id'];
            $cargo->from            = $request->input('sender');
            $cargo->office_id       = $request->input('branch');
            $cargo->office_id_to    = $request->input('branch_target');
            $cargo->for             = $request->input('receiver');
            $cargo->address_for     = $request->input('targets');
            $cargo->in_date         = Carbon::now();
            $cargo->due_date         = $request->input('due_date');

            $cargo->save();

            foreach ($request->goods as $row) {
                $good = new Goods;
                $service = json_decode($row['service']);
                $good->cargo_id     = $cargo->id_cargo;
                $good->service_id   = $service->id_services;
                $good->weight       = $row['weight'];
                $good->description  = $row['desc'];
                $good->quantity     = $row['quantity'];
                $good->price        = $service->prices;

                $good->save();
            }

            $cargos = Cargo::find($cargo->id_cargo);
            $date = Carbon::now();
            $cargos->receipt = $cargo->id_cargo."/".$user['id'].'/'.$date->format('y/m');

            $cargos->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        
        return redirect()->route('load');
    }

    public function received($id){
        DB::beginTransaction();
        try {  
            $cargo = Cargo::find($id);
            $cargo->status = 1;

            $cargo->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }

        return redirect()->route('detail', ['id'=>$id]);
    }


    public function service(){
        $user = request()->auth_user;
        $services = Service::get();        
        return view('service', compact('services', 'user'));
    }

    public function add_service(){
        $user = request()->auth_user;
        $action = route('insert_service');
        $subtitle = 'add';
        return view('crud_service', compact('action', 'subtitle', 'user'));
    }

    public function insert_service(Request $request){
        DB::beginTransaction();
        try {  
            $service = new Service;
            $service->services = $request->input('services');
            $service->prices = $request->input('prices');

            $service->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }       
        return redirect()->route('load_service');
    }

    public function edit_service($id){
        $user = request()->auth_user;
        $services = Service::find($id);        
        $subtitle = 'edit';
        $action = route('update_service', ['id'=>$id] );
        return view('crud_service', compact('services', 'action', 'subtitle', 'user'));
    }

    public function update_service(Request $request, $id){
        DB::beginTransaction();
        try {
            $services = Service::find($id); 

            $services->services = $request->input('services');
            $services->prices = $request->input('prices');

            $services->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }       
        return redirect()->route('load_service');
    }
}
