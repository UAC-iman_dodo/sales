@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">
					<div class="breadcrumb-holder">
                            <h1 class="main-title float-left">Branch</h1>
                            <ol class="breadcrumb float-right">
								<li class="breadcrumb-item active">List Branch</li>
                            </ol>
                            <div class="clearfix"></div>
                    </div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
				<div class="card mb-3">
					<div class="card-header">
					</div>
						
					<div class="card-body">
						<div class="table-responsive">
						<table id="example1" class="table table-bordered table-hover display">
							<thead>
								<tr>
									<th>Kantor</th>
									<th>Status</th>
									<th>Alamat</th>
									<th>Telepon</th>	
									<th>Fax</th>
									<th>Kontak</th>
									<th>Selluler</th>
									<th></th>
								</tr>
							</thead>										
							<tbody>
								@foreach ($office as $offices)
								<tr>
									<td>{{ $offices->office }}</td>
									<td>{{ $offices->status }}</td>
									<td>{{ $offices->address.",".$offices->city.",".$offices->province }}</td>
									<td>{{ $offices->code.$offices->contact }}</td>
									<td>{{ $offices->fax }}</td>
									<td>{{ $offices->phone }}</td>
									<td>{{ $offices->cellular }}</td>
									<td><a href="{{ route('detail_branch', ['id'=> $offices->id_office ]) }}" class="btn btn-primary">Detail</a>
									<a href="{{ route('edit_branch', ['id'=> $offices->id_office ]) }}" class="btn btn-primary">Edit</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
						</div>
						
					</div>														
				</div><!-- end card-->					
			</div>

		</div>
	</div>
	<!-- END container-fluid -->

</div>
@endsection
