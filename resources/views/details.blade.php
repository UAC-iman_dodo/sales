@extends('tempt')

@section('content')

<div class="content">    
	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
                    <h1 class="main-title float-left">Transaksi</h1>
                    <ol class="breadcrumb float-right">
						<li class="breadcrumb-item active">Detail Transaksi</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>

		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-table"></i> Detail</h3>
					</div>
								
					<div class="card-body">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="invoice-title text-center mb-3">
										<h2>Receipt {{ @$cargo->receipt }}</h2>
										<strong>Date:</strong> {{ date("F d, Y", strtotime(@$cargo->in_date)) }}
									</div>
									<hr>
									<div class="row">
										<div class="col-md-6">																
											<h5>Billed To:</h5>
											<address>
												{{ @$cargo->from }}<br>
												{{ @$cargo->address_from }}
											</address>
										</div>
										<div class="col-md-6 float-right text-right">																
											<h5>Shipped To:</h5><br>
											<address>
												{{ @$cargo->for }}<br>
												{{ @$cargo->address_for }}<br>
											</address>
											<h5>Target Estimated:</h5>
												{{ date("F d, Y", strtotime(@$cargo->due_date)) }}
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title"><strong>Barang</strong></h3>
										</div>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-condensed">
													<thead>
														<tr>
															<td><strong>Item</strong></td>
															<td class="text-center"><strong>Price</strong></td>
															<td class="text-center"><strong>Quantity</strong></td>
															<td class="text-right"><strong>Totals</strong></td>
														</tr>
													</thead>
													<tbody>
														<!-- foreach ($order->lineItems as $line) or some such thing here -->
														@if (!empty($cargo->goods))	
														<?php $total = 0; ?>
														@foreach (@$cargo->goods as $product)
															<tr>
																<td>{{ $product->description." - ".$product->weight."/gram" }}</td>
																<td class="text-center">{{ $product->quantity }}</td>
																<td class="text-center">{{ $product->price }}</td>
																<td class="text-right">{{ $product->price*$product->quantity }}</td>
															</tr>
															<?php $total += $product->price*$product->quantity;?>
														@endforeach
														<tr>
															<td class="no-line"></td>
															<td class="no-line"></td>
															<td class="no-line text-center"><strong>Total</strong></td>
															<td class="no-line text-right">{{ $total }}</td>
														</tr>
														<tr style="{{ (@$cargo->status != 1?'':'display: none') }}">
															<td class="no-line"></td>
															<td class="no-line"></td>
															<td class="no-line"></td>
															<td class="no-line text-right"><a href="{{ route('received', ['id'=>$cargo->id_cargo]) }}" class="btn btn-primary">Barang Diterima</a></td>
														</tr>
														@endif
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							  
						
					</div><!-- end card body -->															
							
					</div><!-- end card-->					
					
				</div><!-- end col-->			

		</div><!-- end row-->
    </div>

</div>
	<!-- END container-fluid -->
@endsection