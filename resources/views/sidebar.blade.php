
<div class="left main-sidebar">
	<div class="sidebar-inner leftscroll">
		<div id="sidebar-menu">
			<ul>

		        <li class="smenu">
		        	<!-- add active in class ahref -->
                    <a href="#" class=""><i class="fa fa-cubes bigfonts"></i> <span> Transaksi barang </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled" style="display: block;">
                            <li><a href="{{ route('load') }}">List</a></li>
                            <li style="{{ (@$user['level_id'] == 3?'display: none':'') }}"><a href="{{ route('register') }}">Add</a></li>
                        </ul>
                </li>
                <li class="smenu" style="{{ (@$user['level_id'] == 3?'display: none':'') }}">
                    <a href="#" class=""><i class="fa fa-user-circle bigfonts"></i> <span> Branch management </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled" style="display: block;">
                            <li><a href="{{ route('load_branch') }}">List Cabang</a></li>
                            <li><a href="{{ route('register_branch') }}">Add Cabang</a></li>
                        </ul>
                </li>
		        <li class="smenu" style="{{ (@$user['level_id'] == 3?'display: none':'') }}">
                    <a href="#" class=""><i class="fa fa-user-circle bigfonts"></i> <span> Service </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled" style="display: block;">
                            <li><a href="{{ route('load_service') }}">List Service</a></li>
                            <li><a href="{{ route('add_service') }}">Add Service</a></li>
                        </ul>
                </li>
		        
            </ul>
            <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>