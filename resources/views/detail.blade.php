@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">
					<div class="breadcrumb-holder">
                            <h1 class="main-title float-left">Branch</h1>
                            <ol class="breadcrumb float-right">
								<li class="breadcrumb-item active">Detail Branch</li>
                            </ol>
                            <div class="clearfix"></div>
                    </div>
			</div>
		</div>


		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-table"></i> Detail</h3>
					</div>
								
					<div class="card-body">
						<div class="container">

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title"><strong>Branch</strong></h3>
										</div>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-condensed">
													<tbody>
														<tr>
															<td class="text-right">Cabang :</td>
															<td class="text-left">{{ @$office->office }}</td>
														</tr>
														<tr>
															<td class="text-right">Status :</td>
															<td class="text-left">{{ @$office->status }}</td>
														</tr>
														<tr>
															<td class="text-right">Alamat :</td>
															<td class="text-left">{{ @$office->address }}</td>
														</tr>
														<tr>
															<td class="text-right">Kontak :</td>
															<td class="text-left">{{ @$office->contact }}</td>
														</tr>
														<tr>
															<td class="text-right">Fax :</td>
															<td class="text-left">{{ @$office->fax }}</td>
														</tr>
														<tr>
															<td class="text-right">contact person :</td>
															<td class="text-left">{{ @$office->phone.", ".@$office->cellular }}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				    </div>
    			</div>
		</div><!-- end card body -->															
				
		</div><!-- end card-->					

	</div><!-- end col-->			

</div><!-- end row-->
@endsection