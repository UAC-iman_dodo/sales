<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title>BKM Express</title>
	<meta name="description" content="BKM Express Admin Page | BKM Express">
	<meta name="author" content="Iman">

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/png" href="{{ asset('pike/images/logo.png') }}"/>
	
	<!-- Bootstrap CSS -->
	<link href="{{ asset('pike/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	
	<!-- Font Awesome CSS -->
	<link href="{{ asset('pike/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	
	<!-- Custom CSS -->
	<link href="{{ asset('pike/css/style.css') }}" rel="stylesheet" type="text/css" />

	<!-- Modernizr -->
	<script src="{{ asset('pike/js/modernizr.min.js') }}"></script>

	<!-- jQuery -->
	<script src="{{ asset('pike/js/jquery.min.js') }}"></script>

	<!-- Moment -->
	<script src="{{ asset('pike/js/moment.min.js') }}"></script>
	
	<!-- BEGIN CSS for this page -->
	<link href="{{ asset('pike/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" /> 

	<!-- Switchery css -->
	<link href="{{ asset('pike/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
		
	<style>	
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
			
	#calendar {
		width: 100%;
	}

	.fc-event {
	font-size: 1em;
	border-radius: 2px;
	border: none;
	padding: 5px;
	}
	
	.fc-day-grid-event .fc-content {
	color: #fff;
	}	
	
	.fc-button {
	background: #eaeaea;
	border: none;
	color: #666b6f;
	margin: 0 3px !important;
	padding: 5px 12px !important;    
	text-transform: capitalize;
	height: auto !important;
	box-shadow: none !important;
	border-radius: 3px !important;    
	}
	
	.fc-state-down, .fc-state-active, .fc-state-disabled {
	background-color: #009ffc !important;
	color: #ffffff !important;
	text-shadow: none !important;
	}
	
	.fc-toolbar h2 {
	font-size: 20px;
	font-weight: 600;
	line-height: 28px;
	text-transform: uppercase;
	}
	
	.fc th.fc-widget-header {
	background: #e6e6e6;
	font-size: 13px;
	text-transform: uppercase;
	line-height: 18px;
	padding: 10px 0px;
	}
	
	.fc-unthemed th, .fc-unthemed td, .fc-unthemed thead, .fc-unthemed tbody, .fc-unthemed .fc-divider, .fc-unthemed .fc-row, .fc-unthemed .fc-popover {
	border-color: #eff1f3;
	}
	</style>
	<!-- END CSS for this page -->
			
</head>

<body class="adminbody">

<div id="main">

	<!-- top bar navigation -->
	<div class="headerbar">

		<!-- LOGO -->
        <div class="headerbar-left">
			<a href="index.html" class="logo"><img alt="Logo" src="{{ asset('pike/images/logo.png') }}" /> <span>Admin</span></a>
        </div>

        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0">

                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ asset('pike/images/avatars/admin.png') }}" alt="Profile image" class="avatar-rounded">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Hello, {{@$user['name']}}</small> </h5>
                        </div>
                        
                        <!-- item-->
                        <a href="{{ $broker_logout_url }}" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Logout</span>
                        </a>
                    </div>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left">
						<i class="fa fa-fw fa-bars"></i>
                    </button>
                </li>                        
            </ul>

        </nav>

	</div>
	<!-- End Navigation -->
	
 
	<!-- Left Sidebar -->
    @include('sidebar')
	<!-- End Sidebar -->


    <div class="content-page">
	
		<!-- Start content -->
        @yield('content')
		<!-- END content -->

    </div>
	<!-- END content-page -->
    
	<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="#">BKM Express</a>
		</span>
		<span class="float-right">
		Powered by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
		</span>
	</footer>

</div>
<!-- END main -->

<script src="{{ asset('pike/js/popper.min.js') }}"></script>
<script src="{{ asset('pike/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('pike/js/detect.js') }}"></script>
<script src="{{ asset('pike/js/fastclick.js') }}"></script>
<script src="{{ asset('pike/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('pike/js/jquery.nicescroll.js') }}"></script>

<!-- App js -->
<script src="{{ asset('pike/js/pikeadmin.js') }}"></script>

<!-- BEGIN Java Script for this page -->
<script src="{{ asset('pike/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('pike/plugins/fullcalendar/fullcalendar.min.js') }}"></script>


<script src="{{ asset('pike/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('pike/plugins/switchery/switchery.min.js') }}"></script>

<script>
$(document).ready(function() {	
		/* initialize the external events
		-----------------------------------------------------------------*/
		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});

		/* initialize the calendar
		-----------------------------------------------------------------*/
		var date = new Date();
		var d    = date.getDate();
        var m    = date.getMonth();
        var y    = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				var title = prompt('Event Title:');
				var eventData;
				if (title) {
					eventData = {
						title: title,
						start: start,
						end: end
					};
					$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
				}
				$('#calendar').fullCalendar('unselect');
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
					{
					  title          : 'All Day Event',
					  start          : new Date(y, m, 2),
					  className: 'bg-primary',
					},
					{
					  title          : 'Long Event',
					  start          : new Date(y, m, d - 4),
					  end            : new Date(y, m, d - 2),
					  className: 'bg-info',
					},
					{
					  title          : 'Meeting John',
					  start          : new Date(y, m, d, 15, 20),
					  allDay         : false,
					  className: 'bg-primary',
					},
					{
					  title          : 'New Task',
					  start          : new Date(y, m, d, 12, 0),
					  end            : new Date(y, m, d, 16, 0),
					  allDay         : false,
					  className: 'bg-danger',
					},
					{
					  title          : 'Birthday Party',
					  start          : new Date(y, m, d + 2, 15, 0),
					  end            : new Date(y, m, d + 2, 20, 40),
					  allDay         : false,
					  className: 'bg-warning',
					},
					{
					  title          : 'Alice Birthday',
					  start          : new Date(y, m, d + 4, 15, 0),
					  end            : new Date(y, m, d + 4, 18, 30),
					  allDay         : false,
					  className: 'bg-info',
					},
					{
					  title          : 'Click for Google',
					  start          : new Date(y, m, 27),
					  end            : new Date(y, m, 28),
					  url            : 'http://google.com/',
					  className: 'bg-danger', 
					}
				
			],
			droppable: true, // this allows things to be dropped onto the calendar
			drop: function() {
		
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			}
		});
});
</script>
<!-- END Java Script for this page -->

</body>
</html>