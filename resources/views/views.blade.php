@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
							
						<div class="card-body">
							<div class="table-responsive">
							<table id="example1" class="table table-bordered table-hover display">
								<thead>
									<tr>
										<th>dari</th>
										<th>Alamat asal</th>
										<th>untuk</th>
										<th>Alamat tujuan</th>
										<th>Jumlah Barang</th>
										<th>Tanggal masuk</th>
										<th>batas tanggal terakhir</th>
										<th>Receipt</th>
										<th></th>
									</tr>
								</thead>										
								<tbody>
									@foreach ($cargo as $goods)
									<tr>
										<td>{{ $goods->from }}</td>
										<td>{{ $goods->office->address }}</td>
										<td>{{ $goods->for }}</td>
										<td>{{ $goods->address_for }}</td>
										<td>{{ count($goods->goods) }}</td>
										<td>{{ date("F d, Y", strtotime(@$goods->in_date)) }}</td>
										<td>{{ date("F d, Y", strtotime(@$goods->due_date)) }}</td>
										<td>{{ $goods->receipt }}</td>
										<td><a href="{{ route('detail', ['id'=>$goods->id_cargo]) }}" class="btn btn-primary">Detail</a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
							
						</div>														
					</div><!-- end card-->					
				</div>

			</div>

    </div>
	<!-- END container-fluid -->

</div>
@endsection