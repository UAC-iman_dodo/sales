@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">Branch</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
						<form action="{{$action}}" method="post">
						{{ csrf_field() }}
						<div class="card-body">
							<div class="form-group">
								<label for="branch">Cabang</label>
								<input class="form-control" id="branch" name="branch" required="" type="text" value="{{ @$office->office }}">
							</div>
							<div class="form-group">
								<label for="address">Alamat</label>
								<input class="form-control" id="address" name="address" required="" type="text" value="{{ @$office->address }}">
							</div>
							<div class="form-group">
								<label for="status">Status Cabang</label>
								{{ Form::select('status', $status, @$office->status, ['class'=>'form-control', 'id'=>'status', 'type'=>'text'])}}
								<!-- <input class="form-control" id="status" name="status" required="" type="text" value="{{ @$office->status }}"> -->
							</div>
							<div class="form-group">
								<label for="contact">Kontak</label>
								<input class="form-control" id="contact" name="contact" required="" type="text" value="{{ @$office->contact }}">
							</div>
							<div class="form-group">
								<label for="fax">Fax</label>
								<input class="form-control" id="fax" name="fax" required="" type="text" value="{{ @$office->fax }}">
							</div>
							<div class="form-group">
								<label for="phone">User</label>
								<input class="form-control" id="phone" name="phone" required="" type="text" value="{{ @$office->phone }}">
							</div>
							<div class="form-group">
								<label for="cellular">Seluler</label>
								<input class="form-control" id="cellular" name="cellular" required="" type="text" value="{{ @$office->cellular }}">
							</div>
						  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</div>
						</form>								
					</div><!-- end card-->					
                </div>
			</div>
    </div>
	<!-- END container-fluid -->

</div>
@endsection