@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">Service</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
						<form action="{{$action}}" method="post">
						{{ csrf_field() }}
						<div class="card-body">
							<div class="form-group">
								<label for="services">Nama Service</label>
								<input class="form-control" id="services" name="services" required="" type="text" value="{{ @$services->services }}">
							</div>
							<div class="form-group">
								<label for="prices">Harga</label>
								<input class="form-control" id="prices" name="prices" required="" type="text" value="{{ @$services->prices }}">
							</div>
						  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</div>
						</form>								
					</div><!-- end card-->					
                </div>
			</div>
    </div>
	<!-- END container-fluid -->

</div>
@endsection