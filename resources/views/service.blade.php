@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
							
						<div class="card-body">
							<div class="table-responsive">
							<table id="example1" class="table table-bordered table-hover display">
								<thead>
									<tr>
										<th>Service</th>
										<th>Harga</th>
										<th></th>
									</tr>
								</thead>										
								<tbody>
									@foreach ($services as $service)
									<tr>
										<td>{{ $service->services }}</td>
										<td>{{ $service->prices }}</td>
										<td><a href="{{ route('edit_service', ['id'=>$service->id_services]) }}" class="btn btn-primary">Edit</a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
							
						</div>														
					</div><!-- end card-->					
				</div>

			</div>

    </div>
	<!-- END container-fluid -->

</div>
@endsection