@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">Barang</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
						<form action="{{$action}}" method="post">
						{{ csrf_field() }}
						<div class="card-body">
							<div class="form-group">
								<label for="branch">kantor cabang</label>
								{{ Form::select('branch', $office, @$office->id_office, ['class'=>'form-control', 'id'=>'status', 'type'=>'text'])}}
								<!-- <input class="form-control" id="branch" name="branch" required="" type="text" value="{{ @$cargo->address }}"> -->
							</div>
							<div class="form-group">
								<label for="sender">Nama pengirim</label>
								<input class="form-control" id="sender" name="sender"  required="" type="text" value="{{ @$cargo->from }}">
							</div>
							<div class="form-group">
								<label for="receiver">Nama penerima</label>
								<input class="form-control" id="receiver" name="receiver"  required="" type="text" value="{{ @$cargo->for }}">
							</div>
							<div class="form-group">
								<label for="branch">kantor cabang tujuan</label>
								{{ Form::select('branch_target', $office, @$office->id_office, ['class'=>'form-control', 'id'=>'branch', 'type'=>'text'])}}
								<!-- <input class="form-control" id="branch" name="branch_target" required="" type="text" value="{{ @$cargo->address }}"> -->
							</div>
							<div class="form-group">
								<label for="targets">Alamat Tujuan</label>
								<input class="form-control" id="targets" name="targets"  required="" type="text" value="{{ @$cargo->address_for }}">
							</div>
							<div class="form-group">
								<label for="due_date">Tanggal target penerimaan</label>
								{{ Form::date('due_date',\Carbon\Carbon::now(), ['class'=>'form-control'])}}
								<!-- <input class="form-control" id="due_date" name="due_date"  required="" type="text" value="{{ @$cargo->address_for }}"> -->
							</div>
						</div>
						<div class="card-body">
				            <div class="table-responsive">  
				                <table class="table table-bordered" id="dynamic_field">  
				                    <tr>  
				                        <td><input type="text" name="goods[0][desc]" placeholder="deskripsi" class="form-control name_list" /></td>
				                        <td><input type="number" name="goods[0][quantity]" placeholder="jumlah" class="form-control name_list" /></td>
				                        <td><input type="number" name="goods[0][weight]"" placeholder="Berat/gram" class="form-control name_list" /></td>  
										<td>{{ Form::select('goods[0][service]', $service, null, ['class'=>'form-control name_list',  'type'=>'text'])}}</td>  

				                        <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
				                    </tr>  
				                </table>  
				            </div>
						  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</div>
						</form>					
					</div><!-- end card-->					
                </div>
			</div>
    </div>
	<!-- END container-fluid -->

</div>
<script type="text/javascript">
$(document).ready(function(){      

    var postURL = "<?php echo url('addmore'); ?>";
    var i=1;


    $('#add').click(function(){  

        $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="goods['+i+'][desc]" placeholder="deskripsi" class="form-control name_list" /></td><td><input type="number" name="goods['+i+'][quantity]" placeholder="jumlah" class="form-control name_list" /></td><td><input type="number" name="goods['+i+'][weight]" placeholder="Berat/gram" class="form-control name_list" /></td><td><select class="form-control name_list" name="goods['+i+'][service]" >@foreach($services as $row)<option value="{{ json_encode($row) }}">{{ $row->services }}</option>@endforeach()</select></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>')
        i++;
    });  


    $(document).on('click', '.btn_remove', function(){  
		button_id = $(this).attr("id");   
		$('#row'+button_id+'').remove();  
    });  


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});


    $('#submit').click(function(){            

        $.ajax({  
            url:postURL,  
            method:"POST",  
            data:$('#add_name').serialize(),
            type:'json',
            success:function(data){

                if(data.error){
                    printErrorMsg(data.error);
                }else{
                    i=1;
                    $('.dynamic-added').remove();
                    $('#add_name')[0].reset();
                    $(".print-success-msg").find("ul").html('');
                    $(".print-success-msg").css('display','block');
                    $(".print-error-msg").css('display','none');
                    $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                }
            }  

        });  

    });  


    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $(".print-success-msg").css('display','none');

        $.each( msg, function( key, value ) {
        	$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
});  

</script>
@endsection